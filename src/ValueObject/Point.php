<?php

/*
 * This file is part of the doctrine-spatial package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Doctrine\Spatial\ValueObject;

/**
 * This class defines model for WGS84 coordinates.
 */
class Point
{
    private float $latitude;
    private float $longitude;

    /**
     * @param float $longitude
     * @param float $latitude
     */
    public function __construct(float $longitude, float $latitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function toArray(): array
    {
        return [
            $this->longitude,
            $this->latitude,
        ];
    }

    public function toWKT(): string
    {
        return sprintf('POINT(%11.7F %11.7F)', $this->getLongitude(), $this->getLatitude());
    }

    public static function fromWKT(string $wkt): self
    {
        [$longitude, $latitude] = sscanf($wkt, 'POINT(%f %f)');

        return new static((float)$longitude, (float)$latitude);
    }
}
