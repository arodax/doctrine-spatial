<?php

/*
 * This file is part of the doctrine-spatial package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Doctrine\Spatial\ValueObject;

/**
 * This class defines model for WGS84 coordinates.
 */
class MultiPolygon
{
    private const PATTERN = '/\(\(((\s*-?\d+(\.\d+)*\s+-?\d+(\.\d+)*\s*)(,\s*-?\d+(\.\d+)*\s+-?\d+(\.\d+)*\s*)*)\)\)/';

    /**
     * @var array
     */
    private array $polygons = [];

    /**
     * @param array $polygons
     */
    public function __construct(array $polygons)
    {
        $this->polygons =   $polygons;
    }

    /**
     * @var array
     * @return array
     */
    public function getPolygons(): array
    {
        return $this->polygons;
    }

    public function toArray(): array
    {
        return $this->polygons;
    }

    public function toWKT(): string
    {
        $string = '';
        foreach ($this->getPolygons()[0] as $polygon) {
            $substring = '';

            foreach($polygon[0] as $pair) {
                $substring .= implode(' ', $pair).', ';
            }

            $string .= '(('. rtrim($substring, ', ') .')),';
        }

        $string = rtrim($string, ',');
        $string = "MULTIPOLYGON({$string})";

        return $string;
    }

    public static function fromWKT(string $wkt): self
    {
        //'MULTIPOLYGON(((45.55215127678357 -122.65701296506451, 45.52329405876074 -122.63572695432232, 45.52473727138698 -122.56156923947856, 45.54397656857749 -122.56088259397076, 45.559363267325914 -122.60345461545514, 45.56224780438123 -122.65220644650982, 45.55215127678357 -122.65701296506451)),((45.55215127678357 -122.65701296506451, 45.52329405876074 -122.63572695432232, 45.52473727138698 -122.56156923947856, 45.54397656857749 -122.56088259397076, 45.559363267325914 -122.60345461545514, 45.56224780438123 -122.65220644650982, 45.55215127678357 -122.65701296506451)))'
        preg_match_all(self::PATTERN, $wkt, $matches, PREG_SET_ORDER);

        $multipolygon = [];
        foreach($matches as $match) {
            $match = $match[1];
            $match = explode(',', $match);

            $polygon = [];
            foreach($match as $pair) {
                $pair = explode(' ', trim($pair));
                $polygon[] = [$pair[0], $pair[1]];
            }

            $multipolygon[] = [$polygon];
        }

        return new static($multipolygon ?? []);
    }
}


/*
        $matches = [];
        $pattern = '/\(\(((\s*-?\d+(\.\d+)*\s+-?\d+(\.\d+)*\s*)(,\s*-?\d+(\.\d+)*\s+-?\d+(\.\d+)*\s*)*)\)\)/';
        $subject = 'MULTIPOLYGON(((45.55215127678357 -122.65701296506451, 45.52329405876074 -122.63572695432232, 45.52473727138698 -122.56156923947856, 45.54397656857749 -122.56088259397076, 45.559363267325914 -122.60345461545514, 45.56224780438123 -122.65220644650982, 45.55215127678357 -122.65701296506451)))';
        $subject = 'MULTIPOLYGON(((0 0,11 0,12 11,0 9,0 0)),((3 5,7 4,4 7,7 7,3 5)))';
        $subject = 'MULTIPOLYGON(((45.55215127678357 -122.65701296506451, 45.52329405876074 -122.63572695432232, 45.52473727138698 -122.56156923947856, 45.54397656857749 -122.56088259397076, 45.559363267325914 -122.60345461545514, 45.56224780438123 -122.65220644650982, 45.55215127678357 -122.65701296506451)),((45.55215127678357 -122.65701296506451, 45.52329405876074 -122.63572695432232, 45.52473727138698 -122.56156923947856, 45.54397656857749 -122.56088259397076, 45.559363267325914 -122.60345461545514, 45.56224780438123 -122.65220644650982, 45.55215127678357 -122.65701296506451)))';

        preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

        echo '<pre>';
        var_dump($matches);
        echo '</pre>';

        $multipolygon = [];
        foreach($matches as $match) {
            $match = $match[1];
            $match = explode(',', $match);

            $polygon = [];
            foreach($match as $pair) {
                $pair = explode(' ', trim($pair));
                $polygon[] = [$pair[0], $pair[1]];
            }

            $multipolygon[] = $polygon;
        }

        echo '<pre>';
        var_dump($multipolygon);
        echo '</pre>';

        $string = '';
        foreach ($multipolygon as $polygon) {
            $substring = '';

            foreach($polygon as $pair) {
                $substring .= implode(' ', $pair).', ';
            }

            $string .= '(('. rtrim($substring, ', ') .')),';
        }

        $string = rtrim($string, ',');
        $string = "MULTIPOLYGON({$string})";

        echo '<pre>';
        var_dump($string);
        var_dump($subject);
        var_dump($string === $subject);
        echo '</pre>';
 */