<?php

/*
 * This file is part of the doctrine-spatial package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Doctrine\Spatial\ValueObject;

/**
 * This class defines model for WGS84 coordinates.
 */
class Polygon
{
    /**
     * @var array|Point[]
     */
    private array $points = [];

    /**
     * [
     *  [50.859819, 5.708942],
     * ]
     *
     * @param array $points
     */
    public function __construct(array $points)
    {
        foreach ($points as $point) {
            $this->points[] =   new Point((float)$point[0], (float)$point[1]);
        }
    }

    /**
     * @var array|Point[]
     * @return array
     */
    public function getPoints(): array
    {
        return $this->points;
    }

    public function toArray(): array
    {
        $arr = [];

        foreach ($this->points as $point) {
            $arr[] = $point->toArray();
        }

        return $arr;
    }

    public function toWKT(): string
    {
        $arr = $this->getPoints();

        foreach ($arr as &$val) {
            $val = implode(' ', $val->toArray());
        }

        $str = implode(',', $arr);
        $str = 'POLYGON(('. $str .'))';

        return $str;
    }

    public static function fromWKT(string $wkt): self
    {
        //'POLYGON((50.866753 5.686455, 50.859819 5.708942, 50.851475 5.722675, 50.841611 5.720615, 50.834023 5.708427, 50.840744 5.689373, 50.858735 5.673923, 50.866753 5.686455))'
        $str = $wkt;
        $str = substr($str, 9);
        $str = substr($str, 0, strlen($str) - 2);
        $arr = explode(',', $str);

        foreach ($arr as $key => &$val) {
            $val = trim($val);
            $val = explode(' ', $val);
        }

        return new static($arr ?? []);
    }
}
