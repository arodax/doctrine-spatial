<?php

/*
 * This file is part of the doctrine-spatial package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Doctrine\Spatial\Utils;

use Arodax\Doctrine\Spatial\ValueObject\MultiPolygon;
use Arodax\Doctrine\Spatial\ValueObject\Point;
use Arodax\Doctrine\Spatial\ValueObject\Polygon;
use Doctrine\DBAL\Connection;
use Brick\Geo;

class WithinHelper
{
    public function __construct(Connection $conn)
    {
        if(!class_exists(Geo\Engine\GeometryEngineRegistry::class)) {
            throw new \RuntimeException('you need to install brick/geo to use this class');
        }

        Geo\Engine\GeometryEngineRegistry::set(new Geo\Engine\PDOEngine($conn->getWrappedConnection()));
    }

    public function within($needle, $haystack): bool
    {
        if ($haystack instanceof Point) {
            if ($needle instanceof Point) {
                return $this->pointWithinPoint($needle, $haystack);
            }
        }

        if ($haystack instanceof Polygon) {
            if ($needle instanceof Point) {
                return $this->pointWithinPolygon($needle, $haystack);
            }

            if ($needle instanceof Polygon) {
                return $this->polygonWithinPolygon($needle, $haystack);
            }
        }

        if ($haystack instanceof MultiPolygon) {
            if ($needle instanceof Point) {
                return $this->pointWithinMultiPolygon($needle, $haystack);
            }

            if ($needle instanceof Polygon) {
                return $this->polygonWithinMultiPolygon($needle, $haystack);
            }
        }

        throw new \InvalidArgumentException();
    }

    public function pointWithinPoint(Point $needle, Point $haystack): bool
    {
        $haystack = Geo\Point::fromText($haystack->toWKT());
        $needle = Geo\Point::fromText($needle->toWKT());

        return $haystack->equals($needle);
    }

    public function pointWithinPolygon(Point $needle, Polygon $haystack): bool
    {
        $haystack = Geo\Polygon::fromText($haystack->toWKT());
        $needle = Geo\Point::fromText($needle->toWKT());

        return $haystack->contains($needle);
    }

    public function pointWithinMultiPolygon(Point $needle, MultiPolygon $haystack): bool
    {
        $haystack = Geo\MultiPolygon::fromText($haystack->toWKT());
        $needle = Geo\Point::fromText($needle->toWKT());

        return $haystack->contains($needle);
    }

    public function polygonWithinPolygon(Polygon $needle, Polygon $haystack): bool
    {
        $haystack = Geo\Polygon::fromText($haystack->toWKT());
        $needle = Geo\Polygon::fromText($needle->toWKT());

        return $haystack->contains($needle);
    }

    public function polygonWithinMultiPolygon(Polygon $needle, MultiPolygon $haystack): bool
    {
        $haystack = Geo\MultiPolygon::fromText($haystack->toWKT());
        $needle = Geo\Polygon::fromText($needle->toWKT());

        return $haystack->contains($needle);
    }
}